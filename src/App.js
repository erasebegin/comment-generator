import React, { useState, useEffect } from "react";
import { UserContext } from "./UserContext";
import { Switch, Route } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Signup from "./pages/Signup";
import NewComment from "./pages/NewComment";
import ViewComments from "./pages/ViewComments";
import Navbar from "./components/Navbar";

import "./App.css";

function App() {
  const [user, setUser] = useState([]);
  const [token, setToken] = useState("");
  const [data, setData] = useState([]);
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  //check local storage for token and retrieve
  useEffect(() => {
    if (localStorage.getItem("userToken")) {
      setToken(JSON.parse(localStorage.getItem("userToken")));
      setIsLoggedIn(true);
    }
  }, []);

  //check if token exists in state, if so add to localStorage
  useEffect(() => {
    if (token) {
      localStorage.setItem("userToken", JSON.stringify(token));
      fetchData(token);
    }
  }, [token]);

  //handle log and sign up in event
  const login = (val) => {
    setUser(val.user);
    setToken(val.token);
    setIsLoggedIn(true);
    fetchData(token);
  };

  const logout = () => {
    setIsLoggedIn(false);
    localStorage.clear();
  };

  //fetch comment data for authorized user
  const fetchData = async (userToken) => {
    setIsLoading(true)
    var reqHeaders = new Headers();
    reqHeaders.append("Authorization", `Bearer ${userToken}`);
    reqHeaders.append("Content-Type", "application/json");
    await fetch(process.env.REACT_APP_SERVER_ADDRESS+"/comments", {
      mode: "cors",
      method: "GET",
      headers: reqHeaders,
    })
      .then((response) => response.json())
      .then((result) => {
        setData(result);
        setIsLoading(false)
      })
      .catch((error) => console.log("error", error));
  };

  //in case user returns to site after clearing component state, fetch data again witch cached token
  useState(() => {
    if (isLoggedIn) {
      fetchData(token);
    }
  }, []);

  return (
    <UserContext.Provider
      value={{ login, logout, user, token, data, fetchData, isLoggedIn, isLoading }}
    >
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/signup" component={Signup} />
        <Route path="/add_comment" component={NewComment} />
        <Route path="/view_comments" component={ViewComments} />
      </Switch>
    </UserContext.Provider>
  );
}

export default App;
