const grammarData = {
  excellent: [
    "Fantastic, didn't notice any problems in this lesson",
    "No issues in this lesson",
    "Displayed superb grammar knowledge",
    "Did not notice any issues with grammar in this lesson",
    "Grammar was outstanding",
    "Super, no problems in this class",
    "Wonderful grammar wizard",
    "Super duper grammar trouper!"
  ],
  good: [
    "Not many problems, but still needs to focus on verb conjugation",
    "Forms sentences well with some minor issues",
    "Good grammar but we are still working on conjugating those verbs",
    "Focused on verb conjugation. Grammar was otherwise good",
    "Verb conjugation is still a bit of an issue, but grammar knowledge was good overall",
    "There were some issues with verb conjugation, but grammar was otherwise good",
    "Conjugating verbs for third person needs work",
    "Nothing significant to point out for this class",
    "Great grammar, just needs to work on forming complete sentences including both subject and object",
    "Very good, just a few things that need to be adjusted",
    "Not bad at all, we're almost ready to move on to some more complicated concepts such as the present perfect tense",
    "Strong performance, just needs tweaking in one or two areas"
  ],
  average: [
    "Did not notice any systematic errors in this lesson",
    "Struggles a bit with choosing the correct tense when talking about the past",
    "Not too many mistakes, but needs to improve in a few areas",
    "Was able to understand sentences, but struggled to make their own",
    "Still making slips when using verbs in the past tense",
    "Not too bad, but there are one or two things that we need to still focus on",
    "Pretty good, but there are lots of things that we still need to work on"
  ],
  "below average": [
    "Is able to form some basic sentences, but still needs to learn many foundation structures",
    "Does fine with very basic sentence structures such as [subject][verb] or [subject][verb][object], but needs to work on understanding and using more complex structures"
  ],
  poor: [
    "Very basic understanding of grammatical concepts",
    "Barely able to construct basic sentences",
    "Does not know how to form basic sentences",
    "Does not yet know foundational structures",
    "Does not meet the basic requirements for language comprehension"
  ]
};

export default grammarData;
