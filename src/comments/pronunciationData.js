const pronunciationData = {
  excellent: [
    "Outstanding pronunciation",
    "Pronunciation was superb",
    "Fantastic, didn't notice any problems in this lesson",
    "Pronunciation was not far from that of a native speaker, as was fluency",
    "Pronunciation was brilliant",
    "Terrific, very little that needs improving",
    "Already close to perfect, this is not an area of concern",
  ],
  good: [
    "Good pronunciation overall, just needs to work on fluency",
    "No specific problems with pronunciation in this lesson",
    'Good pronunciation, but needs to focus on saying "the"',
    "Good pronunciation, definitely shows improvement",
    "Good pronunciation, seems to be improving consistently",
    "Good pronunciation, but we are still ironing out some issues",
    "Good pronunciation, just needs a little work",
    "Clearly has a good amount of practice with pronunciation",
    "Good pronunciation, no significant issues to point out for this lesson",
    "Good pronunciation, just needs a little refinement",
    "Very good overall, but there are certain sounds that HE/SHE still struggles with",
    "Great pronunciation, just needs to be tweaked here and there",
    "Not bad at all, just needs to work on a few areas",
  ],
  average: [
    "Good pronunciation, but room for improvement",
    "Quite good pronunciation, but there is certainly room for improvement",
    "Pronunciation was fine, but there are several things that need practice",
    "Not bad pronunciation, but needs to keep practicing",
    "Neither good nor bad, there is still much to improve",
    "Acceptable, but pronunciation is still quite far from natural",
    "There is still a fair bit of work to be done here",
  ],
  "below average": [
    "Plenty of errors, but good in some areas",
    "Plenty that needs improving",
    "Really needs to focus on improving pronunciation",
    "Pronunciation is an area of concern",
    "Pronunciation needs wuite a bit of work",
    "Quite a basic understanding of phonics so far, still plenty of work to be done in this area",
    "Still fairly weak in this area, we will have to focus on pronunciation in future"
  ],
  poor: [
    "Needs a lot of practice",
    "Absolute beginner, pronunciation needs a lot of work",
    "Difficult to understand, pronunciation is something that needs a lot of work",
    "This is an area that still needs a lot of work",
    "Little or no understanding of English language phonemes, we'll have to work hard to improve pronunciation",
    "This is the area that needs the most improvement. Watching English TV shows is a good way to boost pronunciation"
  ],
};

export default pronunciationData;
