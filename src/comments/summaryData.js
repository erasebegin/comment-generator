const summaryData = {
  excellent: [
    "A great lesson, we had a lot of fun and learned plenty too!",
    "A super productive lesson where we learned a lot and enjoyed ourselves too",
    "A lovely lesson, we got through a lot of content today",
    "Great lesson, very focused and productive",
    "Overall excellent, the lesson went very smoothly and we learned a lot",
    "We got through lots of content and had a very enjoyable lesson overall",
    "A great lesson that was as fun as it was informative",
    "A wonderful lesson, we had a great time learning and laughing",
    "A super productive and entertaining lesson",
    "I hope every lesson can be as educational and fun as this one",
    "Another great lesson, [NAME] was alert, curious and relaxed-- the perfect way to learn!"
  ],
  good: [
    "A good lesson, we learned plenty of new words and practiced pronunciation too",
    "Good performance, we learned quite a bit today",
    "Lesson went well and we covered quite a lot of material",
    "Quite a focused lesson, we learned plenty and had a good discussion too",
    "A solid lesson, we got through all of the material and even did some revision",
    "A focused lesson getting through all the material with enough time for a fun discussion in between",
    "Lesson went smoothly, we covered a lot and had a discussion too",
    "Overall the lesson went well and was quite enjoyable for us both.",
    "An enjoyable lesson getting through quite a lot of content.",
    "A pretty fun lesson with a few interesting facts to keep us going",
    "Even though the text was difficult we made very good progress",
    "The text was easy, so we explored a few additional concepts"
  ],
  average: [
    "The lesson was somewhat productive, but things did not go all that smoothly",
    "A pretty good lesson, we got through the material just fine",
    "Good behaviour and a productive lesson overall",
    "The lesson went fine, but not as well as I had hoped",
    "Acceptable overall, but I expected more",
    "Not a bad lesson, but things did not go all that smoothly",
    "Pretty good, but I hope that our lessons can go a little more smoothly in future"
  ],
  "below average": [
    "This lesson went fairly well, but we did not make as much progress I had hoped",
    "We covered a lot of the material, but I feel that the lesson was not as productive as it could have been",
    "Unfortunately the lesson did not go particularly well",
    "Unfortunately this lesson was not particularly productive or enjoyable"
  ],
  poor: {}
};

export default summaryData;
