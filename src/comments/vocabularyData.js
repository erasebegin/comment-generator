const vocabularyData = {
  excellent: [
    "Very wide vocabulary knowledge",
    "Vocabulary was more than sufficient to explore the text thoroughly",
    "Very wide vocabulary, no problems with even the more difficult words",
    "Super vocabulary knowledge, didn't need much help from the teacher",
    "Very few words were provided, already very familiar with most of the text's vocabulary",
    "Great vocabulary knowledge, very self-reliant in understanding the text",
    "Most words were already understood very well so we just focused on one or two of the trickier words",
    "Didn't encounter any unknown words, except for perhaps the word ____"
  ],
  good: [
    "Good vocabulary knowledge, not many words needed to be provided by the teacher",
    "Quite a wide vocabulary, there were not many words that needed to be explained",
    "Didn't need much help with new vocabulary",
    "Knows plenty of words already, but we still learned some new ones",
    "Learned lots of new words, but enough was understood to have a good discussion",
    "The vocabulary in this lesson was quite easy, there wasn't much that needed to be learned to understand the text",
    "We learned a few new words, but vocabulary was mostly understood already, allowing us to explore additional topics",
    "Good vocabulary knowledge, enough to engage in meaningful discussion about the text",
    "Not many words needed to be provided in order to understand the text",
    "Vocabulary is quite broad for HIS/HER level"
  ],
  average: [
    "Pretty good vocabulary knowledge, but there is room for improvement",
    "We learned quite a few new words in order to tackle this text",
    "Looked at some new key words and reviewed a few older ones",
    "Recognised a few important words, but many words were new",
    "Was familiar with quite a few of the words we encountered, but still had plenty of new words to learn"
  ],
  "below average": [
    "Some words were well understood, but we spent a lot of time introducing new words in order to understand the text",
    "Some knowledge of key words, but there were a lot of new words that needed to be understood before the meaning of the text was clear",
    "Very limited vocabulary, but just about enough to work through the material",
    "There were a lot of words that had to be taught before the material could be adequately comprehended"
  ],
  poor: [
    "The text is clearly too difficult, there were far too many words that were not understood",
    "This text was not suitable for the learner level, too many words needed to be explained",
    "The lesson moved very slowly as too many words needed to be explained, this text is not the right level",
    "Learned some new words, but general understanding was poor. Please choose an easier text next time"
  ]
};

export default vocabularyData;
