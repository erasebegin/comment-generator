import React, { useState, useContext } from "react";
import GradeButtons from "./GradeButtons";
import styled from "styled-components";
import { UserContext } from "../UserContext";
import ClipLoader from "react-spinners/ClipLoader";

export default function AddComment() {
  const [type, setType] = useState("");
  const [grade, setGrade] = useState("");
  const [description, setDescription] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [isValid, setIsValid] = useState(true);

  const { token, fetchData } = useContext(UserContext);

  const handleGradeSet = (gradeInput) => {
    setGrade(gradeInput);
  };

  const handleTypeSet = (title) => {
    setType(title);
  };

  const checkValid = (e) => {
    if (type === "" || grade === "") {
      setIsValid(false);
    } else {
      addComment(e);
    }
  };

  const addComment = async (e) => {
    e.preventDefault();

    if (isValid) {
      setIsLoading(true);
      const reqHeaders = new Headers();
      reqHeaders.append("Authorization", `Bearer ${token}`);
      reqHeaders.append("Content-Type", "application/json");

      await fetch(process.env.REACT_APP_SERVER_ADDRESS + "/comments", {
        mode: "cors",
        method: "POST",
        body: JSON.stringify({ description, type, grade }),
        headers: reqHeaders,
      })
        .then((response) => response.json())
        .then((result) => {
          setIsLoading(false);
          fetchData(token);
          setDescription("");
        })
        .catch((error) => console.log("error", error));
    }
  };

  return (
    <Container isValid={isValid} className="add-comment-container">
      <Title>Add Comment</Title>
      <div className="section-title-buttons">
        <Button
          onClick={() => handleTypeSet("pronunciation")}
          active={type === "pronunciation" ? true : false}
        >
          pronunciation
        </Button>
        <Button
          onClick={() => handleTypeSet("vocabulary")}
          active={type === "vocabulary" ? true : false}
        >
          vocabulary
        </Button>
        <Button
          onClick={() => handleTypeSet("grammar")}
          active={type === "grammar" ? true : false}
        >
          grammar
        </Button>
        <Button
          onClick={() => handleTypeSet("summary")}
          active={type === "summary" ? true : false}
        >
          summary
        </Button>
      </div>
      <GradeButtons
        title={type}
        setGrade={handleGradeSet}
        color="white"
        bgColor="#FFFFFF33"
      />
      <textarea
        onChange={(e) => setDescription(e.target.value)}
        value={description}
      />
      {isLoading ? <ClipLoader color="white" /> : ""}
      <p className="error-message">Please choose a type and grade</p>
      <Button onClick={(e)=>checkValid(e)} active="true">
        ADD
      </Button>
      <div className="help-text">
        <h3>Use codes to insert name and gender.</h3>
        <p>
          Type <b>{"[N]"}</b> where you would like the student's name to appear.
        </p>
        <p>
          Type <b>{"[G]"}</b> where you would like the student's gender to
          appear (i.e. he/she).
        </p>
        <p>
          For example if you type: "[N]'s grammar knowledge is outstanding and
          [G] did especially well today."
          <br />
          The name and gender will be automatically filled in when generating
          comments, so it will appear as "Garry's grammar knowledge is
          outstanding and he did especially well today."
        </p>
      </div>
    </Container>
  );
}

const Container = styled.div`
  padding: 1em 2em;
  margin: 2em auto;
  width: 80vw;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1), 0 3px 3px rgba(0, 0, 0, 0.14);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #5e6472;
  font-family: "Nunito", sans-serif;

  @media (min-width: 900px) {
    width: 50vw;
  }

  .help-text {
    padding: 2em;
    margin: 2em auto;
    background: #b8f2e6;
    box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1), 0 3px 3px rgba(0, 0, 0, 0.14);
  }

  .error-message {
    display: ${({ isValid }) => (isValid ? "none" : "initial")};
    color: #ffcccc;
    font-weight: bold;
  }
`;

const Title = styled.h1`
  color: white;
  font-weight: 400;
`;

const Button = styled.button`
  padding: 0.5em;
  margin: 0.5em 0.75em;
  background: ${(props) => (props.active ? "white" : "#FFFFFF33")};
  border: 1px solid white;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
  transition: ease-in-out 200ms;
  color: ${({ active }) => (active ? "black" : "white")};

  &:focus {
    border: 1px solid white;
    outline: none;
  }
  &:focus {
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: ease-in-out 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
