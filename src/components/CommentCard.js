import React from "react";
import CopyButton from "./CopyButton";
import Buttons from "./GradeButtons";
import styled from "styled-components";

export default function CommentCard({ comment, grade, title }) {
  return (
    <Container className="section">
      <div className="header">
        <h3>{title.charAt(0).toUpperCase() + title.slice(1)}</h3>
        {comment ? <CopyButton comment={comment} /> : <div></div>}
      </div>
      <Buttons setGrade={grade} title={title} />
      <div className="comment-container">
        <p>{comment}</p>
      </div>
    </Container>
  );
}

const Container = styled.div`
  font-family: "Nunito", sans-serif;
  h3 {
    font-size: 1.4rem;
    font-weight: 100;
  }
  p {
    font-weight: 100;
    color: black;
  }
`;
