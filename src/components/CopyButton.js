import React from "react";
import styled from "styled-components";
import { ImCopy } from "react-icons/im";

export default function CopyButton({ comment }) {
  const handleClick = () => {
    navigator.clipboard.writeText(comment);
  };

  return (
    <div>
      <Button onClick={handleClick}>
        <ImCopy />
      </Button>
    </div>
  );
}

const Button = styled.button`
  padding: 0.5em;
  margin: .5em .75em;
  background: ${props => (props.active ? "white" : "#FFFFFF88")};
  border: 1px solid white;
  box-shadow: 0 2px 5px rgba(0,0,0,0.19), 0 1px 1px rgba(0,0,0,0.23);
  transition: 200ms;

  &:focus{
    border: 1px solid white;
    outline: none;
  }
  &:focus{
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
