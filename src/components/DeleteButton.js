import React, { useContext, useState } from "react";
import styled from "styled-components";
import { FaRegTrashAlt } from "react-icons/fa";
import { UserContext } from "../UserContext";
import ClipLoader from "react-spinners/ClipLoader";

export default function DeleteButton({ commentId, removeComment }) {
  const { token } = useContext(UserContext);

  const [isLoading, setIsLoading] = useState(false);
  const [isDeleted, setIsDeleted] = useState(false);

  const deleteComment = async () => {
    setIsLoading(true);
    removeComment(commentId);

    const reqHeaders = new Headers();
    reqHeaders.append("Authorization", `Bearer ${token}`);
    await fetch(
      `${process.env.REACT_APP_SERVER_ADDRESS}/comments/${commentId}`,
      {
        mode: "cors",
        method: "DELETE",
        headers: reqHeaders,
      }
    )
      .then((result) => {
        console.log("successfully deleted");
        setIsLoading(false);
        setIsDeleted(true);
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <Button onClick={() => deleteComment()} isDeleted={isDeleted}>
      {isLoading ? <ClipLoader /> : <FaRegTrashAlt />}
    </Button>
  );
}

const Button = styled.a`
  display: ${({ isDeleted }) => (isDeleted ? "none" : "flex")};
  padding: 0 0.5em;
  align-items: center;
  margin-left: 0.75em;
  background: rgba(255, 255, 255, 0.5);
  transition: 200ms;

  &:focus {
    border: 1px solid white;
    outline: none;
  }
  &:focus {
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
