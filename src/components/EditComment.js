import React, { useState, useContext, useRef, useEffect } from "react";
import { AiOutlineSave } from "react-icons/ai";
import ClipLoader from "react-spinners/ClipLoader";
import styled from "styled-components";

import { UserContext } from "../UserContext";

export default function EditComment({ prevComment, commentId, reset }) {
  const { token, isLoading, fetchData } = useContext(UserContext);
  const [comment, setComment] = useState(prevComment);
  const [charCount, setCharCount] = useState(0);

  const focusField = useRef(null);
  const saveButton = useRef(null);

  const handleClickOutside = (e) => {
    if (focusField && !focusField.current.contains(e.target)) {
      if (saveButton && !saveButton.current.contains(e.target)) {
        reset();
      }
    }
  };

  useEffect(() => {
    focusField.current.focus();
    setCharCount(comment.length);
    window.addEventListener("mouseup", handleClickOutside);

    return () => {
      window.removeEventListener("mouseup", handleClickOutside);
    };
  }, [focusField]);

  const updateComment = async () => {
    const reqHeaders = new Headers();
    reqHeaders.append("Authorization", `Bearer ${token}`);
    reqHeaders.append("Content-Type", "application/json");

    await fetch(
      `${process.env.REACT_APP_SERVER_ADDRESS}/comments/${commentId}`,
      {
        mode: "cors",
        method: "PATCH",
        body: JSON.stringify({ description: comment }),
        headers: reqHeaders,
      }
    )
      .then((result) => {
        fetchData(token);
        reset();
      })
      .catch((error) => console.log("error", error));
  };

  if (isLoading) {
    return <ClipLoader />;
  }

  return (
    <Container charCount={charCount}>
      <input
        onChange={(e) => setComment(e.target.value)}
        value={comment}
        ref={focusField}
      ></input>
      <button onClick={updateComment} ref={saveButton}>
        <AiOutlineSave size="1.5em" />
      </button>
    </Container>
  );
}

const Container = styled.div`
  input {
    padding: 0.8em;
    background: rgba(255, 255, 255, 0.7);
    width: ${({ charCount }) => charCount * 7 + "px"};
    font-family: "Nunito", sans-serif;
    font-weight: 100;
    font-size: 1em;

    &:focus {
      border: 2px solid white;
      outline: none;
    }
  }

  button {
    padding: 0.9em 0.8em;
    align-items: center;
    margin-left: 0.75em;
    background: rgba(255, 255, 255, 0.5);
    transition: 200ms;
    border: none;

    &:focus {
      border: 1px solid white;
      outline: none;
    }

    &:hover {
      transition: 200ms;
      transform: scale(1.04);
      background: white;
    }
  }
`;
