import React, { useState } from "react";
import styled from "styled-components";

export default function Buttons({ setGrade, title, color, bgColor }) {
  const [active, setActive] = useState("");

  const fontColor = color ? color : "#5e6472";
  const backgroundColor = bgColor ? bgColor : "#FFFFFF88";

  const handleClick = (grade) => {
    setGrade(grade, title);
    setActive(grade);
  };

  return (
    <div>
      <Button
        onClick={() => handleClick("excellent")}
        active={active === "excellent" ? true : false}
        color={fontColor}
        bgColor={backgroundColor}
      >
        excellent
      </Button>
      <Button
        onClick={() => handleClick("good")}
        active={active === "good" ? true : false}
        color={fontColor}
        bgColor={backgroundColor}

      >
        good
      </Button>
      <Button
        onClick={() => handleClick("average")}
        active={active === "average" ? true : false}
        color={fontColor}
        bgColor={backgroundColor}

      >
        average
      </Button>
      <Button
        onClick={() => handleClick("below average")}
        active={active === "below average" ? true : false}
        color={fontColor}
        bgColor={backgroundColor}
      >
        below average
      </Button>
      <Button
        onClick={() => handleClick("poor")}
        active={active === "poor" ? true : false}
        color={fontColor}
        bgColor={backgroundColor}
      >
        poor
      </Button>
    </div>
  );
}

const Button = styled.button`
  padding: 0.5em;
  margin: 0.5em 0.75em;
  background: ${({active, bgColor}) => (active ? "white" : bgColor)};
  border: 1px solid white;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
  transition: ease-in-out 200ms;
  font-weight: 400;
  color: ${({ color, active }) => active ? "black" : color};

  &:focus {
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: ease-in-out 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
