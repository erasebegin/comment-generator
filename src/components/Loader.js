import React from "react";
import { ClipLoader } from "react-spinners";
import styled from "styled-components";

export default function Loader() {
  return (
    <Container>
      <ClipLoader />
      <p>Loading</p>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50% -50%);
`;
