import React, { useContext } from "react";
import styled from "styled-components";
import { UserContext } from "../UserContext";
import { Route } from "react-router-dom";

export default function LogoutButton() {
  const { logout, token } = useContext(UserContext);

  const doLogout = async () => {
    const reqHeaders = new Headers();
    reqHeaders.append("Authorization", `Bearer ${token}`);

    await fetch(`${process.env.REACT_APP_SERVER_ADDRESS}/users/logout`, {
      mode: "cors",
      method: "POST",
      headers: reqHeaders,
    })
      .then((result) => {
        console.log({ result });
        if (result.ok) {
          logout();
        }
      })
      .catch((error) => console.log("error", error));
  };

  return (
    <Route
      render={({ history }) => (
        <Button
          onClick={() => {
            history.push("/login");
            doLogout();
          }}
        >
          Log out
        </Button>
      )}
    />
  );
}

const Button = styled.a`
  padding: 0.5em;
  margin: 0.5em 0.75em;
  background: ${(props) => (props.active ? "white" : "#FFFFFF88")};
  border: 1px solid white;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
  transition: 200ms;

  &:focus {
    border: 1px solid white;
    outline: none;
  }
  &:focus {
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
