import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { AiOutlineHome, AiOutlineMenu } from "react-icons/ai";
import LogoutButton from "../components/LogoutButton";
import { UserContext } from "../UserContext";

export default function Navbar() {
  const { isLoggedIn } = useContext(UserContext);
  const [isOpen, setIsOpen] = useState(false);

  if (isLoggedIn) {
    return (
      <Container isOpen={isOpen}>
        <button
          onClick={() => {
            setIsOpen(!isOpen);
          }}
          className="menu-button"
        >
          <AiOutlineMenu />
        </button>
        <div className="inner-container">
          <Link
            to="/"
            className="nav-link"
            onClick={() => {
              setIsOpen(false);
            }}
          >
            <AiOutlineHome />
          </Link>
          <div className="middle-buttons">
            <Link
              to="/add_comment"
              className="nav-link"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              Add Comment
            </Link>
            <Link
              to="/view_comments"
              className="nav-link"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              View Comments
            </Link>
          </div>
          <LogoutButton />
        </div>
      </Container>
    );
  }
  return <div></div>;
}

const Container = styled.div`
  padding-top: 2em;
  @media (min-width: 700px) {
    padding-top: 0;
  }

  .inner-container {
    display: flex;
    align-items: center;
    justify-content: center;
    transform: ${(props) => (props.isOpen ? "initial" : "translateX(-100%)")};
    position: fixed;
    top: 0;
    left: 0;
    background: white;
    height: 100%;
    width: 100%;
    z-index: 100;
    flex-direction: column;
    transition: ease-in-out 200ms;

    @media (min-width: 700px) {
      height: auto;
      flex-direction: row;
      justify-content: space-between;
      transform: initial;
      position: initial;
      z-index: 1;
      background: none;
    }
    .middle-buttons {
      display: flex;
      flex-direction: column;
      @media (min-width: 700px) {
        flex-direction: row;
      }
    }
  }

  .nav-link {
    padding: 0.5em;
    margin: 0.5em 0.75em;
    background: ${(props) => (props.active ? "white" : "#FFFFFF88")};
    border: 1px solid white;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
    transition: 200ms;
    text-decoration: none;
    color: black;

    &:focus {
      border: 1px solid white;
      outline: none;
    }
    &:focus {
      border: 1px solid white;
      outline: none;
    }

    &:hover {
      transition: 200ms;
      transform: scale(1.04);
      background: white;
    }

    .align-right {
      justify-self: flex-end;
    }
  }

  .menu-button {
    position: absolute;
    top: 0;
    left: 0;
    margin: 1em;
    z-index: 800;
    padding: 0.5em;
    margin: 0.5em 0.75em;
    background: ${(props) => (props.active ? "white" : "#FFFFFF88")};
    border: 1px solid white;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
    transition: 200ms;
    text-decoration: none;
    color: black;
    font-size: 1.2rem;

    &:focus {
      border: 1px solid white;
      outline: none;
    }
    &:focus {
      border: 1px solid white;
      outline: none;
    }

    &:hover {
      transition: 200ms;
      transform: scale(1.04);
      background: white;
    }
    @media (min-width: 700px) {
      display: none;
    }
  }
`;
