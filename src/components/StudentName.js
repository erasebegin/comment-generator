import React from "react";
import styled from "styled-components";
import { FaMale, FaFemale } from "react-icons/fa";

export default function StudentName({ setName, setGender, name, gender }) {
  return (
    <>
      <Container>
        <NameInput
          type="text"
          onChange={(e) => setName(e.target.value)}
          value={name}
          placeholder="student name"
        />
        <Button
          active={gender === "M" ? true : false}
          onClick={() => setGender("M")}
        >
          <FaMale />
        </Button>
        <Button
          active={gender === "F" ? true : false}
          onClick={() => setGender("F")}
        >
          <FaFemale />
        </Button>
      </Container>
      <h2>
        {name}
        {gender ? gender === "M" ? <FaMale /> : <FaFemale /> : ""}
      </h2>
    </>
  );
}

const Container = styled.div`
  display: flex;
  margin: auto;
  margin-top: 2em;
  align-items: flex-end;
  justify-content: center;
`;

const NameInput = styled.input`
  padding: 0.5em;
  text-align: center;
`;

const Button = styled.button`
  padding: 0.5em;
  margin-left: 0.75em;
  background: ${(props) => (props.active ? "white" : "#FFFFFF88")};
  border: 1px solid white;
  box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
  transition: ease-in-out 200ms;

  &:focus {
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: ease-in-out 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
