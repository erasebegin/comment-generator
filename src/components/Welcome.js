import React from "react";
import styled from "styled-components";

export default function Welcome() {
  return (
    <Container>
      <h1>Welcome to the Student Comment Generator</h1>
      <h3>
        Create a database of student comments and recall them quickly and
        conveniently according to student performance.
      </h3>
      <p>
        This tool was designed specifically for use with the Talk915 online ESL
        platform. If you would like to request any modifications, feel free to
        contact me at{" "}
        <a href="mailto:chris@erasegegin.net">chris@erasebegin.net</a>.
      </p>
    </Container>
  );
}

const Container = styled.div`
  padding: 2em;
  margin: 2em auto;
  background: #b8f2e6;
  box-shadow: 0 5px 10px rgba(0,0,0,0.10), 0 3px 3px rgba(0,0,0,0.14);
`;
