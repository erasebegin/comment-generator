import {useState} from "react"

const useLogin = (email, password) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false)
    fetch("http://localhost:3001/users/login", {
      mode: "cors",
      method: "POST",
      body: JSON.stringify({ email, password }),
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((result) => {
        if(result.user){
          setIsLoggedIn(true)
        }
      })
      .catch((error) => console.log("error", error));

      return isLoggedIn
};

export default useLogin;
