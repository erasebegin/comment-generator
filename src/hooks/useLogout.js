import { useState } from "react";

export default function useLogout() {
  const [isLoggedOut, setIsLoggedOut] = useState(false);
  var reqHeaders = new Headers();
  reqHeaders.append("Authorization", `Bearer ${userToken}`);

  fetch("http://localhost:3001/users/logout", {
    mode: "cors",
    method: "POST",
    body: JSON.stringify({ email, password }),
    headers: reqHeader,
  })
    .then((response) => response.json())
    .then((result) => {
      if (result.user) {
        setIsLoggedOut(true);
      }
    })
    .catch((error) => console.log("error", error));

  return isLoggedOut;
}
