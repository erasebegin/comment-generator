import React, { useState, useContext } from "react";
import { Redirect } from "react-router-dom";

import CommentCard from "../components/CommentCard";
import StudentName from "../components/StudentName";
import Loader from "../components/Loader";
import generateRandom from "../utils/generateRandom";
import replaceCodes from "../utils/replaceCodes";
import { UserContext } from "../UserContext";

function Home() {
  const { isLoggedIn, data, isLoading } = useContext(UserContext);

  const [pronunciationComment, setPronunciationComment] = useState("");
  const [vocabularyComment, setVocabularyComment] = useState("");
  const [grammarComment, setGrammarComment] = useState("");
  const [summaryComment, setSummaryComment] = useState("");
  const [studentName, setStudentName] = useState("");
  const [studentGender, setStudentGender] = useState("");

  const generateComment = (grade, title) => {
    const filteredByTitle = data.filter((item) => {
      return item.type === title;
    });

    const filteredByGrade = filteredByTitle.filter((item) => {
      return item.grade === grade;
    });

    let commentOutput = "Empty";

    if (filteredByGrade.length > 0) {
      commentOutput = filteredByGrade[generateRandom(filteredByGrade.length)];
    }

    const { description } = commentOutput;
    const replacedString = description
      ? replaceCodes(description, studentName, studentGender)
      : "Empty";

    switch (title) {
      case "pronunciation":
        setPronunciationComment(replacedString);
        copyText(replacedString);
        break;
      case "vocabulary":
        setVocabularyComment(replacedString);
        copyText(replacedString);
        break;
      case "grammar":
        setGrammarComment(replacedString);
        copyText(replacedString);
        break;
      case "summary":
        setSummaryComment(replacedString);
        copyText(replacedString);
        break;
      default:
        return;
    }
  };

  const copyText = (text) => {
    navigator.clipboard.writeText(text);
  };

  const setName = (name) => {
    setStudentName(name);
  };

  const setGender = (gender) => {
    setStudentGender(gender);
  };

  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  }

  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className="App">
      <StudentName
        name={studentName}
        gender={studentGender}
        setName={setName}
        setGender={setGender}
      />
      <CommentCard
        comment={pronunciationComment}
        grade={generateComment}
        title="pronunciation"
      />
      <CommentCard
        comment={vocabularyComment}
        grade={generateComment}
        title="vocabulary"
      />
      <CommentCard
        comment={grammarComment}
        grade={generateComment}
        title="grammar"
      />
      <CommentCard
        comment={summaryComment}
        grade={generateComment}
        title="summary"
      />
    </div>
  );
}

export default Home;
