import React, { useState, useContext } from "react";
import { UserContext } from "../UserContext";
import { Redirect, Link } from "react-router-dom";
import styled from "styled-components";
import ClipLoader from "react-spinners/ClipLoader";

import Welcome from "../components/Welcome";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const { login, isLoggedIn } = useContext(UserContext);

  const sendCredentials = async (e) => {
    setIsLoading(true)
    e.preventDefault();
    await fetch(process.env.REACT_APP_SERVER_ADDRESS+"/users/login", {
      mode: "cors",
      method: "POST",
      body: JSON.stringify({ email, password }),
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => response.json())
      .then((result) => {
        if (result.user) {
          login(result);
          setIsLoading(false)
        }
      })
      .catch((error) => console.log("error", error));
  };

  if (isLoggedIn) {
    return <Redirect to="/" />;
  }

  return (
    <Container>
      <form>
        <label>Email</label>
        <input
          onChange={(e) => {
            return setEmail(e.target.value);
          }}
          value={email}
          type="email"
        ></input>
        <label>Password</label>
        <input
          onChange={(e) => {
            return setPassword(e.target.value);
          }}
          value={password}
          type="password"
        ></input>
        <div className="button-container">
          <button onClick={sendCredentials}>Log in</button>
          <Link to="/signup">
            <button>Sign up</button>
          </Link>
        </div>
      </form>
      {isLoading ? <ClipLoader color="white" /> : ""}
      <Welcome />
    </Container>
  );
}

const Container = styled.div`
  padding: 1em 2em;
  margin: 2em auto;
  width: 80vw;
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1), 0 3px 3px rgba(0, 0, 0, 0.14);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #5e6472;

  @media (min-width: 900px) {
    width: 50vw;
  }

  form {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    label {
      color: white;
      margin-bottom: .5em;
    }

    input {
      padding: .5em;
      margin-bottom: .5em;
    }

    button {
      padding: 0.5em;
      margin: 0.5em 0.75em;
      background: white;
      border: 1px solid white;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
      transition: 200ms;

      &:focus {
        border: 1px solid white;
        outline: none;
      }
      &:focus {
        border: 1px solid white;
        outline: none;
      }

      &:hover {
        transition: 200ms;
        transform: scale(1.04);
        background: white;
      }
    }
  }
`;
