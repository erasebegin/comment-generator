import React, { useContext, useState } from "react";
import { UserContext } from "../UserContext";
import styled from "styled-components";
import { Redirect } from "react-router-dom";
import { HiOutlinePencilAlt } from "react-icons/hi";

import DeleteButton from "../components/DeleteButton";
import EditComment from "../components/EditComment";
import Loader from "../components/Loader";

export default function ViewComments() {
  const { data, isLoggedIn } = useContext(UserContext);
  const [current, setCurrent] = useState(0);
  const [idToRemove, setIdToRemove] = useState("");
  const [idToEdit, setIdToEdit] = useState("");

  

  const removeComment = (id) => {
    setIdToRemove(id);
  };

  const mapComments = (type, grade) => {
    if (data.length > 0) {
      return data
        .filter((item) => item.type === type && item.grade === grade)
        .map((item, index) => {
          if (idToEdit === item._id) {
            return (
              <EditComment
                commentId={item._id}
                prevComment={item.description}
                key="editing"
                reset={() => setIdToEdit("")}
              />
            );
          } else {
            return (
              <div className="list-item-container" key={index}>
                <ListItem
                  isRemoved={idToRemove === item._id ? true : false}
                >
                  {item.description}
                </ListItem>
                <DeleteButton
                  commentId={item._id}
                  removeComment={removeComment}
                />
                <Button onClick={() => setIdToEdit(item._id)}>
                  <HiOutlinePencilAlt size="1.5em" />
                </Button>
              </div>
            );
          }
        });
    }
    return <div>No comments to display</div>;
  };

  if (!isLoggedIn) {
    return <Redirect to="/login" />;
  }

  const CategoryPanelComponent = () => {
    return (
      <CategoryPanel>
        <button className="category-button" onClick={() => setCurrent(0)}>
          Pronunciation
        </button>
        <button className="category-button" onClick={() => setCurrent(1)}>
          Vocabulary
        </button>
        <button className="category-button" onClick={() => setCurrent(2)}>
          Grammar
        </button>
        <button className="category-button" onClick={() => setCurrent(3)}>
          Summary
        </button>
      </CategoryPanel>
    );
  };
  if (!data || data.length === 0) {
    console.log("triggered loader");
    return <Loader />;
  }

  return (
    <>
      <CategoryPanelComponent />
      <Container current={current}>
        <div className="section pronunciation">
          <h1>Pronunciation</h1>
          <h2>Excellent</h2>
          {mapComments("pronunciation", "excellent")}
          <h2>Good</h2>
          {mapComments("pronunciation", "good")}
          <h2>Average</h2>
          {mapComments("pronunciation", "average")}
          <h2>Below Average</h2>
          {mapComments("pronunciation", "below average")}
          <h2>Poor</h2>
          {mapComments("pronunciation", "poor")}
        </div>
        <div className="section vocabulary">
          <h1>Vocabulary</h1>
          <h2>Excellent</h2>
          {mapComments("vocabulary", "excellent")}
          <h2>Good</h2>
          {mapComments("vocabulary", "good")}
          <h2>Average</h2>
          {mapComments("vocabulary", "average")}
          <h2>Below Average</h2>
          {mapComments("vocabulary", "below average")}
          <h2>Poor</h2>
          {mapComments("vocabulary", "poor")}
        </div>
        <div className="section grammar">
          <h1>Grammar</h1>
          <h2>Excellent</h2>
          {mapComments("grammar", "excellent")}
          <h2>Good</h2>
          {mapComments("grammar", "good")}
          <h2>Average</h2>
          {mapComments("grammar", "average")}
          <h2>Below Average</h2>
          {mapComments("grammar", "below average")}
          <h2>Poor</h2>
          {mapComments("grammar", "poor")}
        </div>
        <div className="section summary">
          <h1>Summary</h1>
          <h2>Excellent</h2>
          {mapComments("summary", "excellent")}
          <h2>Good</h2>
          {mapComments("summary", "good")}
          <h2>Average</h2>
          {mapComments("summary", "average")}
          <h2>Below Average</h2>
          {mapComments("summary", "below average")}
          <h2>Poor</h2>
          {mapComments("summary", "poor")}
        </div>
      </Container>
    </>
  );
}

const ListItem = styled.li`
  display: ${({ isRemoved }) => (isRemoved ? "none" : "initial")};
  list-style: none;
  font-weight: 100;
  padding: 1em;
  background: rgba(255, 255, 255, 0.5);
`;

const CategoryPanel = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 2em;
  font-family: 'Nunito', sans-serif;

  .category-button {
    padding: 0.5em;
    margin: 0.5em 0.75em;
    border: 1px solid white;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.19), 0 1px 1px rgba(0, 0, 0, 0.23);
    transition: 200ms;

    &:focus {
      border: 1px solid white;
      outline: none;
    }
    &:focus {
      border: 1px solid white;
      outline: none;
    }

    &:hover {
      transition: 200ms;
      transform: scale(1.04);
      background: white;
    }

    &:nth-child(1) {
      background: #b8f2e6;
    }
    &:nth-child(2) {
      background: #faf3dd;
    }
    &:nth-child(3) {
      background: #aed9e0;
    }
    &:nth-child(4) {
      background: #ffa69e;
    }
  }
`;

const Container = styled.div`
h1 {
  font-weight: 400;
}
  h2 {
    font-weight: 200;
  }
  .pronunciation {
    display: ${(props) => (props.current !== 0 ? "none" : "block")};
  }
  .vocabulary {
    display: ${(props) => (props.current !== 1 ? "none" : "block")};
  }
  .grammar {
    display: ${(props) => (props.current !== 2 ? "none" : "block")};
  }
  .summary {
    display: ${(props) => (props.current !== 3 ? "none" : "block")};
  }

  .list-item-container {
    display: flex;
    padding: 1em 0;
  }

  .pronunciation {
    background: #b8f2e6;
  }

  .vocabulary {
    background: #faf3dd;
  }

  .grammar {
    background: #aed9e0;
  }

  .summary {
    background: #ffa69e;
  }
`;

const Button = styled.button`
  display: ${({ isDeleted }) => (isDeleted ? "none" : "flex")};
  padding: 0 0.5em;
  align-items: center;
  margin-left: 0.75em;
  background: rgba(255, 255, 255, 0.5);
  transition: 200ms;
  border: none;

  &:focus {
    border: 1px solid white;
    outline: none;
  }
  &:focus {
    border: 1px solid white;
    outline: none;
  }

  &:hover {
    transition: 200ms;
    transform: scale(1.04);
    background: white;
  }
`;
