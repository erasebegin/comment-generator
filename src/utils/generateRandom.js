export default function generateRandom(length) {
  let rand = Math.floor(Math.random() * length);
  let prevRand;
  if (rand !== prevRand) {
    prevRand = rand;
  } else {
    rand = Math.floor(Math.random() * length);
  }

  return rand;
}
