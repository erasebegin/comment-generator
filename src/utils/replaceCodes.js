// helper function to replace static [NAME] with student name variable

export default function replaceCodes (string, name, gender){
    const replacedName = string.replace("[N]",name);
    const replaceGenderCaps = replacedName.replace(". [G]", gender==="M"?". He":". She");
    const replaceGenderLower = replaceGenderCaps.replace("[G]", gender==="M"?"he":"she");
    const replacePossessiveCaps = replaceGenderLower.replace(". [P]", gender==="M"?". His":". Her");
    const replacePossessiveLower = replacePossessiveCaps.replace("[P]", gender==="M"?"his":"her");

    return replacePossessiveLower
} 